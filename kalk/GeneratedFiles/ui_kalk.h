/********************************************************************************
** Form generated from reading UI file 'kalk.ui'
**
** Created by: Qt User Interface Compiler version 5.7.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_KALK_H
#define UI_KALK_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLCDNumber>
#include <QtWidgets/QPushButton>

QT_BEGIN_NAMESPACE

class Ui_kalkClass
{
public:
    QPushButton *pushButton;
    QPushButton *pushButton_2;
    QPushButton *pushButton_3;
    QPushButton *pushButton_4;
    QPushButton *pushButton_5;
    QPushButton *pushButton_6;
    QPushButton *pushButton_7;
    QPushButton *pushButton_8;
    QPushButton *pushButton_9;
    QPushButton *pushButton_10;
    QPushButton *pushButton_11;
    QPushButton *pushButton_12;
    QPushButton *pushButton_13;
    QPushButton *pushButton_14;
    QPushButton *pushButton_15;
    QPushButton *pushButton_16;
    QPushButton *pushButton_17;
    QPushButton *pushButton_18;
    QPushButton *pushButton_19;
    QPushButton *pushButton_20;
    QLCDNumber *lcdNumber;

    void setupUi(QDialog *kalkClass)
    {
        if (kalkClass->objectName().isEmpty())
            kalkClass->setObjectName(QStringLiteral("kalkClass"));
        kalkClass->resize(470, 708);
        pushButton = new QPushButton(kalkClass);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(360, 600, 91, 91));
        QFont font;
        font.setPointSize(24);
        pushButton->setFont(font);
        pushButton_2 = new QPushButton(kalkClass);
        pushButton_2->setObjectName(QStringLiteral("pushButton_2"));
        pushButton_2->setGeometry(QRect(240, 500, 91, 91));
        QFont font1;
        font1.setPointSize(22);
        pushButton_2->setFont(font1);
        pushButton_3 = new QPushButton(kalkClass);
        pushButton_3->setObjectName(QStringLiteral("pushButton_3"));
        pushButton_3->setGeometry(QRect(130, 500, 91, 91));
        pushButton_3->setFont(font1);
        pushButton_4 = new QPushButton(kalkClass);
        pushButton_4->setObjectName(QStringLiteral("pushButton_4"));
        pushButton_4->setGeometry(QRect(20, 500, 91, 91));
        pushButton_4->setFont(font1);
        pushButton_5 = new QPushButton(kalkClass);
        pushButton_5->setObjectName(QStringLiteral("pushButton_5"));
        pushButton_5->setGeometry(QRect(20, 390, 91, 91));
        pushButton_5->setFont(font1);
        pushButton_6 = new QPushButton(kalkClass);
        pushButton_6->setObjectName(QStringLiteral("pushButton_6"));
        pushButton_6->setGeometry(QRect(130, 390, 91, 91));
        pushButton_6->setFont(font1);
        pushButton_7 = new QPushButton(kalkClass);
        pushButton_7->setObjectName(QStringLiteral("pushButton_7"));
        pushButton_7->setGeometry(QRect(240, 390, 91, 91));
        pushButton_7->setFont(font1);
        pushButton_8 = new QPushButton(kalkClass);
        pushButton_8->setObjectName(QStringLiteral("pushButton_8"));
        pushButton_8->setGeometry(QRect(360, 280, 91, 91));
        pushButton_8->setFont(font1);
        pushButton_9 = new QPushButton(kalkClass);
        pushButton_9->setObjectName(QStringLiteral("pushButton_9"));
        pushButton_9->setGeometry(QRect(20, 280, 91, 91));
        pushButton_9->setFont(font1);
        pushButton_10 = new QPushButton(kalkClass);
        pushButton_10->setObjectName(QStringLiteral("pushButton_10"));
        pushButton_10->setGeometry(QRect(130, 280, 91, 91));
        pushButton_10->setFont(font1);
        pushButton_11 = new QPushButton(kalkClass);
        pushButton_11->setObjectName(QStringLiteral("pushButton_11"));
        pushButton_11->setGeometry(QRect(240, 280, 91, 91));
        pushButton_11->setFont(font1);
        pushButton_12 = new QPushButton(kalkClass);
        pushButton_12->setObjectName(QStringLiteral("pushButton_12"));
        pushButton_12->setGeometry(QRect(360, 180, 91, 91));
        pushButton_12->setFont(font1);
        pushButton_13 = new QPushButton(kalkClass);
        pushButton_13->setObjectName(QStringLiteral("pushButton_13"));
        pushButton_13->setGeometry(QRect(240, 180, 91, 91));
        pushButton_13->setFont(font1);
        pushButton_14 = new QPushButton(kalkClass);
        pushButton_14->setObjectName(QStringLiteral("pushButton_14"));
        pushButton_14->setGeometry(QRect(20, 600, 91, 91));
        pushButton_14->setFont(font1);
        pushButton_15 = new QPushButton(kalkClass);
        pushButton_15->setObjectName(QStringLiteral("pushButton_15"));
        pushButton_15->setGeometry(QRect(130, 600, 91, 91));
        pushButton_15->setFont(font1);
        pushButton_16 = new QPushButton(kalkClass);
        pushButton_16->setObjectName(QStringLiteral("pushButton_16"));
        pushButton_16->setGeometry(QRect(360, 390, 91, 91));
        pushButton_16->setFont(font1);
        pushButton_17 = new QPushButton(kalkClass);
        pushButton_17->setObjectName(QStringLiteral("pushButton_17"));
        pushButton_17->setGeometry(QRect(130, 180, 91, 91));
        pushButton_17->setFont(font1);
        pushButton_18 = new QPushButton(kalkClass);
        pushButton_18->setObjectName(QStringLiteral("pushButton_18"));
        pushButton_18->setGeometry(QRect(360, 500, 91, 91));
        pushButton_18->setFont(font1);
        pushButton_19 = new QPushButton(kalkClass);
        pushButton_19->setObjectName(QStringLiteral("pushButton_19"));
        pushButton_19->setGeometry(QRect(240, 600, 91, 91));
        pushButton_19->setFont(font1);
        pushButton_20 = new QPushButton(kalkClass);
        pushButton_20->setObjectName(QStringLiteral("pushButton_20"));
        pushButton_20->setGeometry(QRect(20, 180, 91, 91));
        QFont font2;
        font2.setPointSize(18);
        pushButton_20->setFont(font2);
        lcdNumber = new QLCDNumber(kalkClass);
        lcdNumber->setObjectName(QStringLiteral("lcdNumber"));
        lcdNumber->setGeometry(QRect(20, 40, 421, 121));
        lcdNumber->setDigitCount(8);

        retranslateUi(kalkClass);
        QObject::connect(pushButton_4, SIGNAL(clicked()), kalkClass, SLOT(click_1()));
        QObject::connect(lcdNumber, SIGNAL(overflow()), lcdNumber, SLOT(setDecMode()));
        QObject::connect(pushButton_3, SIGNAL(clicked()), kalkClass, SLOT(click_2()));
        QObject::connect(pushButton_2, SIGNAL(clicked()), kalkClass, SLOT(click_3()));
        QObject::connect(pushButton_5, SIGNAL(clicked()), kalkClass, SLOT(click_4()));
        QObject::connect(pushButton_6, SIGNAL(clicked()), kalkClass, SLOT(click_5()));
        QObject::connect(pushButton_7, SIGNAL(clicked()), kalkClass, SLOT(click_6()));
        QObject::connect(pushButton_9, SIGNAL(clicked()), kalkClass, SLOT(click_7()));
        QObject::connect(pushButton_14, SIGNAL(clicked()), kalkClass, SLOT(click_0()));
        QObject::connect(pushButton_10, SIGNAL(clicked()), kalkClass, SLOT(click_8()));
        QObject::connect(pushButton_11, SIGNAL(clicked()), kalkClass, SLOT(click_9()));
        QObject::connect(pushButton_20, SIGNAL(clicked()), kalkClass, SLOT(click_vyn()));
        QObject::connect(pushButton, SIGNAL(clicked()), kalkClass, SLOT(vysledok()));
        QObject::connect(pushButton_18, SIGNAL(clicked()), kalkClass, SLOT(scitanie()));
        QObject::connect(pushButton_16, SIGNAL(clicked()), kalkClass, SLOT(odcitanie()));
        QObject::connect(pushButton_8, SIGNAL(clicked()), kalkClass, SLOT(nasobenie()));
        QObject::connect(pushButton_12, SIGNAL(clicked()), kalkClass, SLOT(delenie()));
        QObject::connect(pushButton_13, SIGNAL(clicked()), kalkClass, SLOT(odmocnina()));
        QObject::connect(pushButton_17, SIGNAL(clicked()), kalkClass, SLOT(d_mocnina()));
        QObject::connect(pushButton_15, SIGNAL(clicked()), kalkClass, SLOT(desatinna_bodka()));
        QObject::connect(pushButton_19, SIGNAL(clicked()), kalkClass, SLOT(z_znamienka()));

        QMetaObject::connectSlotsByName(kalkClass);
    } // setupUi

    void retranslateUi(QDialog *kalkClass)
    {
        kalkClass->setWindowTitle(QApplication::translate("kalkClass", "kalk", 0));
        pushButton->setText(QApplication::translate("kalkClass", "=", 0));
        pushButton_2->setText(QApplication::translate("kalkClass", "3", 0));
        pushButton_3->setText(QApplication::translate("kalkClass", "2", 0));
        pushButton_4->setText(QApplication::translate("kalkClass", "1", 0));
        pushButton_5->setText(QApplication::translate("kalkClass", "4", 0));
        pushButton_6->setText(QApplication::translate("kalkClass", "5", 0));
        pushButton_7->setText(QApplication::translate("kalkClass", "6", 0));
        pushButton_8->setText(QApplication::translate("kalkClass", "x", 0));
        pushButton_9->setText(QApplication::translate("kalkClass", "7", 0));
        pushButton_10->setText(QApplication::translate("kalkClass", "8", 0));
        pushButton_11->setText(QApplication::translate("kalkClass", "9", 0));
        pushButton_12->setText(QApplication::translate("kalkClass", "/", 0));
        pushButton_13->setText(QApplication::translate("kalkClass", "Sqrt", 0));
        pushButton_14->setText(QApplication::translate("kalkClass", "0", 0));
        pushButton_15->setText(QApplication::translate("kalkClass", ".", 0));
        pushButton_16->setText(QApplication::translate("kalkClass", "-", 0));
        pushButton_17->setText(QApplication::translate("kalkClass", "x^2", 0));
        pushButton_18->setText(QApplication::translate("kalkClass", "+", 0));
        pushButton_19->setText(QApplication::translate("kalkClass", "+/-", 0));
        pushButton_20->setText(QApplication::translate("kalkClass", "Clear", 0));
    } // retranslateUi

};

namespace Ui {
    class kalkClass: public Ui_kalkClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_KALK_H
