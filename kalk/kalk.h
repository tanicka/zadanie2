#ifndef KALK_H
#define KALK_H

#include <QtWidgets/QDialog>
#include "ui_kalk.h"
#include <QToolButton>
#include<QLCDNumber>
#include<QTextStream>
#include<qmessagebox.h>
#include<QLineEdit>


class kalk : public QDialog
{
	Q_OBJECT

public:
	kalk(QWidget *parent = 0);
	~kalk();
	
public slots:
	void click_0();
	void click_1();
	void click_2();
	void click_3();
	void click_4();
	void click_5();
	void click_6();
	void click_7();
	void click_8();
	void click_9();
	
private:
	Ui::kalkClass ui;
	QString hodnotaDisplej;
	double numerickaHodnota;
	int negativne_cislo;
	int desatine_cislo;
	void funkcia(QString x, double y);

	private slots:
		void vysledok();
		void scitanie();
		void odcitanie();
		void nasobenie();
		void delenie();
		void d_mocnina();
		void odmocnina();
		void z_znamienka();
		void click_vyn();
		void desatinna_bodka();
};

#endif // KALK_H