#include "kalk.h"
#include <math.h>

QString cislo="", vysl = "";
double prvec, druhec;
int pocet_nul = 0;
bool scitanieBool = false, odcitanieBool = false, nasobenieBool = false, delenieBool = false, d_mocninaBool = false, odmocninaBool = false, desat_bodka = false, minus=false;

kalk::kalk(QWidget *parent)
	: QDialog(parent)
{
	desatine_cislo = 0;
	negativne_cislo = 0;
	ui.setupUi(this);
}

kalk::~kalk()
{

}

void kalk::vysledok()
{	
	druhec = hodnotaDisplej.toDouble();

	if(scitanieBool)
	{
		vysl = QString::number(prvec + druhec);
		ui.lcdNumber->display(vysl);
	}

	if(odcitanieBool)
	{
		vysl = QString::number(prvec - druhec);
		ui.lcdNumber->display(vysl);
	}

	if (nasobenieBool)
	{
		vysl = QString::number(prvec * druhec);
		ui.lcdNumber->display(vysl);
	}

	if (delenieBool)
	{
		vysl = QString::number(prvec / druhec);
		ui.lcdNumber->display(vysl);
	}

	if (d_mocninaBool)
	{
		vysl = QString::number(prvec * prvec);
		ui.lcdNumber->display(vysl);
	}
	
	if (odmocninaBool)
	{
		vysl = QString::number(sqrt(prvec) );
		ui.lcdNumber->display(vysl);
	}
}

void kalk::scitanie()
{
	prvec = hodnotaDisplej.toDouble();
	hodnotaDisplej = " ";
	desatine_cislo = 0;
	ui.lcdNumber->display(hodnotaDisplej);
	scitanieBool = true;
}

void kalk::odcitanie()
{
	prvec = hodnotaDisplej.toDouble();
	hodnotaDisplej = " ";
	desatine_cislo = 0;
	ui.lcdNumber->display(hodnotaDisplej);
	odcitanieBool = true;
}

void kalk::nasobenie()
{
	prvec = hodnotaDisplej.toDouble();
	hodnotaDisplej = " ";
	desatine_cislo = 0;
	ui.lcdNumber->display(hodnotaDisplej);
	nasobenieBool = true;
}

void kalk::delenie()
{
	prvec = hodnotaDisplej.toDouble();
	hodnotaDisplej = " ";
	desatine_cislo = 0;
	ui.lcdNumber->display(hodnotaDisplej);
	delenieBool = true;
}

void kalk::d_mocnina()
{
	prvec = hodnotaDisplej.toDouble();
	hodnotaDisplej = " ";
	desatine_cislo = 0;
	ui.lcdNumber->display(hodnotaDisplej);
	d_mocninaBool = true;
}

void kalk::odmocnina()
{
	prvec = hodnotaDisplej.toDouble();
	hodnotaDisplej = " ";
	desatine_cislo = 0;
	ui.lcdNumber->display(hodnotaDisplej);
	odmocninaBool = true;
}


void kalk::z_znamienka()
{
	minus = true;
}

void kalk::click_vyn()
{
	cislo = "0";
	ui.lcdNumber->display(cislo);
}

void kalk::desatinna_bodka() 
{
	desat_bodka = true;
}


void kalk::funkcia(QString x, double y)
{
	numerickaHodnota = ui.lcdNumber->value();

	if (desat_bodka == true)
	{
		if (numerickaHodnota >= 0) {
			numerickaHodnota = numerickaHodnota + y / (pow(10, pocet_nul));
			pocet_nul = 0;
		}
		else {
			numerickaHodnota = numerickaHodnota - y / (pow(10, pocet_nul));
			minus = false;
			pocet_nul = 0;
		}
		hodnotaDisplej = QString::number(numerickaHodnota);
		ui.lcdNumber->display(hodnotaDisplej);
		desat_bodka = false;
	}
	else
	{
		if (minus) {
			cislo = "-"+x;					
			minus = false;
		}
		else
			cislo = x;
		numerickaHodnota = ui.lcdNumber->value();
		if (numerickaHodnota != 0)
		{
			hodnotaDisplej = QString::number(numerickaHodnota);
			hodnotaDisplej.append(cislo);
			pocet_nul = 0;
		}
		else
		{
			hodnotaDisplej.clear();
			hodnotaDisplej.append(cislo);
		}
		ui.lcdNumber->display(hodnotaDisplej);
	}
}
	

void kalk::click_1()
{
	QString c = "1";
	double p = 0.1;
	funkcia(c, p);
}


void kalk::click_2()
{
	QString c = "2";
	double p = 0.2;
	funkcia(c, p);
}

 
void kalk::click_3()
{
	QString c = "3";
	double p = 0.3;
	funkcia(c, p);
}

void kalk::click_4()
{
	QString c = "4";
	double p = 0.4;
	funkcia(c, p);
}

void kalk::click_5()
{
	QString c = "5";
	double p = 0.5;
	funkcia(c, p);
}

void kalk::click_6()
{	
	QString c = "6";
	double p = 0.6;
	funkcia(c, p);
}

void kalk::click_7()
{
	QString c = "7";
	double p = 0.7;
	funkcia(c, p);


}

void kalk::click_8()
{
	QString c = "8";
	double p = 0.8;
	funkcia(c, p);
}

void kalk::click_9()
{
	QString c = "9";
	double p = 0.9;
	funkcia(c, p);
}

void kalk::click_0()
{

	if ((desat_bodka == true) || (pocet_nul > 0))
	{ 
		pocet_nul++;
	
	}
	else
	{
		cislo = "0";
		numerickaHodnota = ui.lcdNumber->value();
		if (numerickaHodnota != 0)
		{
			hodnotaDisplej = QString::number(numerickaHodnota);
			hodnotaDisplej.append(cislo);
		}
		else
		{
			hodnotaDisplej.clear();
			hodnotaDisplej.append(cislo);
		}
		ui.lcdNumber->display(hodnotaDisplej);
	}
}


